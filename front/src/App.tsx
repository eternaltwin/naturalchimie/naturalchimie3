import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Home from './routes/Home';
import Playfield from './routes/Game';
import Inventory from './routes/Inventory';
import { CurrentLocationProvider } from 'src/hooks/CurrentLocationContext';
import { UserContextProvider } from './hooks/userContext';

const App: React.FC = () => {
  return (
    <BrowserRouter>
      <UserContextProvider>
        <CurrentLocationProvider>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/act/play" component={Playfield} />
            <Route exact path="/act/bag" component={Inventory} />
          </Switch>
        </CurrentLocationProvider>
      </UserContextProvider>
    </BrowserRouter>
  );
};

export default App;
