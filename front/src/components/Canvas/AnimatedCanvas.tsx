import React, { useRef, useEffect } from 'react';

export interface AnimatedCanvasProps {
  className?: string;
  width: number;
  height: number;
  draw: (context: CanvasRenderingContext2D, frameCount: number) => void;
  playAnimation: boolean;
}

const AnimatedCanvas: React.FC<AnimatedCanvasProps> = (props) => {
  const { draw, playAnimation, ...rest } = props;
  const canvasRef = useRef<HTMLCanvasElement>(null);

  useEffect(() => {
    if (canvasRef.current) {
      const canvas = canvasRef.current;
      const context = canvas.getContext('2d');
      if (context) {
        let frameCount = 0;
        let animationFrameId: number;

        const render = () => {
          frameCount++;
          draw(context, frameCount);
          animationFrameId = window.requestAnimationFrame(render);
        };
        if (playAnimation) {
          render();
        }

        return () => {
          window.cancelAnimationFrame(animationFrameId);
        };
      }
    }
  }, [draw]);

  return <canvas ref={canvasRef} {...rest} />;
};

export default AnimatedCanvas;
