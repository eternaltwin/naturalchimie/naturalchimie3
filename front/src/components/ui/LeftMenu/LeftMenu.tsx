import React from 'react';
import styled from 'styled-components';
import AvatarFrame from './Avatar/Avatar';
import QuestScroll from './QuestScroll';
import SubMenu from './SubMenu/SubMenu';

const StyledMenu = styled.div`
  background-color: #a38c73;
  min-width: 200px;
`;

const StyledAvatarFrame = styled(AvatarFrame)`
  position: relative;
  margin-left: -3px;
  z-index: 0;
`;

const QuestsAndMenus = styled.div`
  position: relative;
  z-index: 1000;
  display: flex;
  justify-content: center;
  margin-top: -10px;
  width: 95%;
  min-width: 200px;
  background-color: #4d4236;
  border-radius: 5px;
  min-height: 300px;
  box-shadow: inset 0px 3px 2px #5d595a, 0px 3px 5px #3c3837;
`;

const StyledSubMenu = styled(SubMenu)`
  position: absolute;
  top: 40px;
  z-index: -2;
`;

interface LeftMenuProps {
  className?: string;
}

const LeftMenu: React.FC<LeftMenuProps> = (props) => {
  return (
    <StyledMenu className={props.className}>
      <StyledAvatarFrame />
      <QuestsAndMenus>
        <QuestScroll />
        <StyledSubMenu />
      </QuestsAndMenus>
    </StyledMenu>
  );
};

export default LeftMenu;
