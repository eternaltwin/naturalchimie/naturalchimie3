import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import assetsDictionnary from 'src/utils/assetsDictionnary';
import { useUserContext } from 'src/hooks/userContext';
import { useIntl } from 'react-intl';
import { Quest } from 'src/types';
import { useLocale } from 'src/hooks/IntlContext';

const applyGrammar = (input: string, local: string, quests: Quest[]): string => {
  if (/fr(-[A-Z][A-Z])?/.test(local)) {
    return input.replace('(s)', quests.length === 1 ? '' : 's');
  } else {
    return input;
  }
};

const QuestText = styled.div`
  position: absolute;
  text-align: center;
  display: block;
  left: 20%;
  width: 60%;
  p {
    color: #1a1612;
    font-size: 10px;
    line-height: 12px;
    overflow-wrap: break-word;
  }
`;

const StyledContainer = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  height: 50px;
  :after {
    position: absolute;
    content: url(${assetsDictionnary['mainUi.leftMenu.questScroll']});
    z-index: -1;
  }
`;

interface QuestScrollProps {
  className?: string;
}

const QuestScroll: React.FC<QuestScrollProps> = (props) => {
  const { quests } = useUserContext();
  const { locale } = useLocale();
  const { formatMessage } = useIntl();
  const [text, setText] = useState(formatMessage({ id: 'quests.noQuest' }));

  useEffect(() => {
    if (quests.length === 0) {
      setText(formatMessage({ id: 'quests.noQuest' }));
    } else {
      const activeQuest = quests.find((quest) => quest.isActive === true);
      if (activeQuest != undefined) {
        setText(activeQuest.name);
      } else {
        setText(
          applyGrammar(
            formatMessage({ id: 'quests.noActiveQuest' }).replace('#totalQuests', quests.length.toString()),
            locale,
            quests
          )
        );
      }
    }
  }, [quests]);

  return (
    <StyledContainer {...props}>
      <QuestText>
        <p>{text}</p>
      </QuestText>
    </StyledContainer>
  );
};

export default QuestScroll;
