import React from 'react';
import styled from 'styled-components';
import assetsDictionnary from 'src/utils/assetsDictionnary';
import XpFlask from './XpFlask';
import CurrencyCard from '../CurrencyCard';

const StyledXpFlask = styled(XpFlask)`
  position: absolute;
  margin: 50px 80px;
`;

const StyledCurrencies = styled(CurrencyCard)`
  position: absolute;
  top: 0;
  left: 3px;
  z-index: 1;
  width: 100%;
`;

const AvatarFrame: React.FC<{ className?: string }> = (props) => {
  return (
    <div className={props.className}>
      <StyledCurrencies />
      <img src={assetsDictionnary['mainUi.leftMenu.avatarForeground']}></img>
      <StyledXpFlask />
    </div>
  );
};

export default AvatarFrame;
