import React from 'react';
import styled from 'styled-components';
import assetsDictionnary from 'src/utils/assetsDictionnary';
import { ReactComponent as XpFlaskSvg } from 'src/assets/img/ui/leftMenu/XpFlask-Merged.svg';

const StyledFlask = styled.svg`
  #smc {
    opacity: 0;
  }
`;

const XpFlask: React.FC<{ className?: string }> = (props) => {
  console.log(assetsDictionnary['mainUi.leftMenu.xpFlask']);
  return (
    <StyledFlask className={props.className}>
      <svg href={assetsDictionnary['mainUi.leftMenu.xpFlask']} />
    </StyledFlask>
  );
};

export default XpFlask;
