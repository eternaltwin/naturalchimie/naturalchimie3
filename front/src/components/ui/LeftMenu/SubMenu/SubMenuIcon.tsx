import React from 'react';
import { useIntl } from 'react-intl';
import assetsDictionnary from 'src/utils/assetsDictionnary';
import { TooltipBoxWrapper } from '../../Tooltips/TooltipWrapper';
import { menuItemProps } from './SubMenuList';

interface SubMenuProps {
  className?: string;
  menuItem: menuItemProps;
  onClick: React.MouseEventHandler;
}

const SubMenuIcon: React.FC<SubMenuProps> = (props) => {
  const { formatMessage } = useIntl();
  const { menuItem } = props;
  return (
    <TooltipBoxWrapper
      TooltippedElement={
        <img src={assetsDictionnary['mainUi.leftMenu.menu.' + menuItem.name]} onClick={props.onClick} />
      }
      content={formatMessage({ id: 'tooltips.' + menuItem.name })}
      left={menuItem.leftPos}
      top={menuItem.topPos}
    />
  );
};

export default SubMenuIcon;
