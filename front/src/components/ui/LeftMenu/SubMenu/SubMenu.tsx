import React from 'react';
import styled from 'styled-components';
import assetsDictionnary from 'src/utils/assetsDictionnary';
import { useIntl } from 'react-intl';
import SubMenuIcon from './SubMenuIcon';
import SubMenuList, { menuItemProps } from './SubMenuList';
import { useHistory } from 'react-router-dom';

const StyledSubMenu = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  min-height: 250px;
  :after {
    position: absolute;
    content: url(${assetsDictionnary['mainUi.leftMenu.subMenuBackground']});
    z-index: -1;
  }
`;

const SubMenu: React.FC<{ className?: string }> = (props) => {
  const history = useHistory();

  const onClickHandler = (path: string) => {
    history.push(path);
  };

  return (
    <StyledSubMenu {...props}>
      {SubMenuList.map((menuItem, index) => (
        <div key={index} id={menuItem.name}>
          <SubMenuIcon menuItem={menuItem} onClick={() => onClickHandler(menuItem.path)} />
        </div>
      ))}
    </StyledSubMenu>
  );
};

export default SubMenu;
