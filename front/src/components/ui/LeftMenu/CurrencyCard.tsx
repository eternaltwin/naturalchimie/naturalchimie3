import React from 'react';
import { useUserContext } from 'src/hooks/userContext';
import styled from 'styled-components';
import assetsDictionnary from 'src/utils/assetsDictionnary';

const StyledCard = styled.div`
  background-image: url('${assetsDictionnary['mainUi.leftMenu.currencies.background']}');
  background-repeat: no-repeat;
  background-size: stretch;
  height: 3.5em;
  font-weight: bold;
  overflow: hidden;
  z-index: 1;
  transform: translateY(-8px);
  .currencies-wrapper {
    display: flex;
    align-items: center;
    position: relative;
    margin-top: -0.4em;
    height: 100%;
  }
`;

const MoneyCounter = styled.div`
  background-color: #543526;
  border-color: #171816;
  border-radius: 3px;
  border-style: inset;
  border-width: 3px 1px 0.5px;
  color: #edb707;
  font-size: 14px;
  height: 1em;
  min-width: 25%;
  margin-left: 2%;
  text-align: right;
  padding: 0 2px 0 5px;
`;

const MoneyIcon = styled.img`
  margin-left: 3%;
  height: 50%;
`;

const PyramCounter = styled.div`
  background-color: #425058;
  border-color: #171816;
  border-radius: 3px;
  border-style: inset;
  border-width: 3px 1px 0.5px;
  color: #d8f0d6;
  font-size: 14px;
  height: 1em;
  min-width: 20%;
  margin-left: 6%;
  text-align: right;
  padding: 0 2px 0 5px;
`;

const PyramIcon = styled.img`
  margin-left: 2%;
  height: 50%;
  -webkit-transform: scaleX(-1);
  transform: scaleX(-1);
`;

const CurrencyCard: React.FC<{ className?: string }> = ({ className }) => {
  const { currency, pyrams } = useUserContext();
  return (
    <StyledCard className={className}>
      <div className={'currencies-wrapper'}>
        <MoneyIcon src={assetsDictionnary['mainUi.leftMenu.currencies.kubor']} />
        <MoneyCounter>{currency}</MoneyCounter>
        <PyramCounter>{pyrams}</PyramCounter>
        <PyramIcon src={assetsDictionnary['mainUi.leftMenu.currencies.pyram']} />
      </div>
    </StyledCard>
  );
};

export default CurrencyCard;
