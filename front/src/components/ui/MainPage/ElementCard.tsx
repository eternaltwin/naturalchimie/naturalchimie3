import React from 'react';
import { useCurrentLocation } from 'src/hooks/CurrentLocationContext';
import styled from 'styled-components';
import assetsDictionnary from 'src/utils/assetsDictionnary';
import { TooltipBoxWrapper } from '../Tooltips/TooltipWrapper';
import { useIntl } from 'react-intl';

const StyledCard = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, 35px);
  background-image: url(${assetsDictionnary['mainUi.elementsCard']});
  background-color: transparent;
  background-size: contain;
  background-repeat: no-repeat;
  width: 200px;
  position: relative;
  height: 50px;
`;

const StyledImg = styled.img`
  height: 50px;
  width: 50px;
`;

const ElementCard: React.FC = () => {
  const { playerElements } = useCurrentLocation();
  const { formatMessage } = useIntl();
  return (
    <StyledCard>
      {playerElements.slice(-4).map((element, index) => {
        return (
          <div key={index}>
            <TooltipBoxWrapper
              key={index}
              TooltippedElement={<StyledImg src={element.img} />}
              name={element.name}
              content={element.description}
              left={20 + 35 * index}
              top={-6}
            />
          </div>
        );
      })}
    </StyledCard>
  );
};

export default ElementCard;
