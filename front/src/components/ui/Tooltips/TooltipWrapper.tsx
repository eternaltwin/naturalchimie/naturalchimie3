import React, { useState } from 'react';
import styled from 'styled-components';

const TooltipBox = styled.div`
  background-color: #312c26;
  border: solid #7b6a57;
  border-radius: 3px;
  border-width: 2px;
  width: 200px;
  div.tooltiptitle {
    margin: 1px;
    background-color: #675949;
    border: solid #8f7b65;
    border-radius: 2px;
    border-width: 1px;
    color: #dfc8b1;
    font-weight: bold;
    font-size: 10px;
    padding-left: 10px;
  }
  div.tooltipcontent {
    background-color: #312c26;
    border-radius: 2px;
    color: #9b866f;
    font-size: 12px;
    padding: 2px 10px 5px 10px;
  }
`;

const TooltipContainer = styled.div<{ left: number; top: number }>`
  ${(props) => 'left: ' + props.left + 'px'};
  ${(props) => 'top: ' + (props.top + 25) + 'px'};
  position: fixed;
  z-index: 999;
`;

const TooltipWrapperElement = styled.div<{ left?: number; top?: number }>`
  ${(props) => (props.left || props.top ? 'position: absolute' : '')};
  ${(props) => (props.left ? 'left: ' + props.left + 'px' : '')};
  ${(props) => (props.top ? 'top: ' + props.top + 'px' : '')};
`;

interface TooltipWrapperPropsBase {
  TooltippedElement: React.ReactElement;
  left?: number;
  top?: number;
}

interface TooltipWrapperProps extends TooltipWrapperPropsBase {
  Tooltip: React.ReactElement;
}

interface TooltipBoxWrapperProps extends TooltipWrapperPropsBase {
  className?: string;
  content: string;
  name?: string;
}

export const TooltipWrapper: React.FC<TooltipWrapperProps> = ({ TooltippedElement, Tooltip, left, top }) => {
  const [mousePosition, setMousePosition] = useState<{ x: number | null; y: number | null }>({ x: null, y: null });
  const handleHover = function (e: React.MouseEvent<HTMLImageElement, MouseEvent>) {
    setMousePosition({ x: e.clientX, y: e.clientY });
  };
  const handleMouseLeave = function () {
    setMousePosition({ x: null, y: null });
  };

  return (
    <TooltipWrapperElement top={top} left={left}>
      <div style={{ display: 'contents' }} onMouseMove={handleHover} onMouseLeave={handleMouseLeave}>
        {TooltippedElement}
      </div>
      {mousePosition.x !== null && mousePosition.y !== null && (
        <TooltipContainer left={mousePosition.x} top={mousePosition.y}>
          {Tooltip}
        </TooltipContainer>
      )}
    </TooltipWrapperElement>
  );
};

export const TooltipBoxWrapper: React.FC<TooltipBoxWrapperProps> = ({
  TooltippedElement,
  name,
  content,
  left,
  top,
}) => {
  const Tooltip = (
    <TooltipBox>
      {name && <div className={'tooltiptitle'}>{name}</div>}
      <div className={'tooltipcontent'}>{content}</div>
    </TooltipBox>
  );
  return (
    <TooltipWrapper top={top} left={left} TooltippedElement={TooltippedElement} Tooltip={Tooltip}></TooltipWrapper>
  );
};
