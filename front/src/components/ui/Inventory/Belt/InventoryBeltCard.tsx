import React from 'react';
import styled from 'styled-components';
import { useIntl } from 'react-intl';
import ChainedCard from '../../ChainedCard';
import InventoryBelt from './InventoryBelt';

const StyledChainedCard = styled(ChainedCard)`
  font-size: 8pt;
  line-height: 10pt;
  margin-left: 10px;
  padding-left: 10px;
  padding-right: 10px;
  max-width: 90%;
`;

const StyledInventoryBelt = styled(InventoryBelt)`
  padding-bottom: 10px;
`;

interface InventoryBeltProps {
  className?: string;
}

const InventoryBeltCard: React.FC<InventoryBeltProps> = (props) => {
  const { formatMessage } = useIntl();
  return (
    <div className={props.className}>
      <StyledInventoryBelt />
      <StyledChainedCard content={formatMessage({ id: 'inventoryPage.beltDescription' })}></StyledChainedCard>
    </div>
  );
};

export default InventoryBeltCard;
