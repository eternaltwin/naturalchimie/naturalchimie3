import React from 'react';
import styled from 'styled-components';
import assetsDictionnary from 'src/utils/assetsDictionnary';
import InventoryBagBody from './InventoryBagBody';

const StyledBagWrapper = styled.div`
  position: relative;
  margin-left: auto;
  margin-right: auto;
  width: 90%;
  height: 40%;
`;

const BagHeader = styled.img`
  width: 100%;
  min-width: 500px;
  transform: translateY(2px);
`;

const BagFooter = styled.img`
  width: 100%;
  min-width: 500px;
`;

const InventoryBag: React.FC<{ className?: string }> = ({ className }) => {
  return (
    <StyledBagWrapper className={className}>
      <BagHeader src={assetsDictionnary['inventory.bag.bagHeader']} />
      <InventoryBagBody />
      <BagFooter src={assetsDictionnary['inventory.bag.bagFooter']} />
    </StyledBagWrapper>
  );
};

export default InventoryBag;
