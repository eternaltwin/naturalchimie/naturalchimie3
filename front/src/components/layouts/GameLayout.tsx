import React from 'react';
import ElementChain from 'src/components/Tetrice/ElementChain';
import GameContainer from 'src/components/Tetrice/GameContainer';
import GameStateProvider from 'src/hooks/GameStateContext';
import styled from 'styled-components';
import assetsDictionnary from 'src/utils/assetsDictionnary';

const StyledGameLayout = styled.div`
  background-image: url(${assetsDictionnary['game.playBackground']});
  background-color: #1a1712;
  background-repeat: no-repeat;
  background-position: top;
  min-height: 100vh;
  min-width: 100vw;
`;

const ContainerRow = styled.div`
  align-items: stretch;
  display: flex;
  flex-grow: 1;
  flex-basis: 100%;
  flex-wrap: nowrap;
  justify-content: center;
`;

const GameLayout: React.FC = () => {
  return (
    <StyledGameLayout>
      <ContainerRow>
        <ElementChain />
        <GameStateProvider>
          <GameContainer />
        </GameStateProvider>
      </ContainerRow>
    </StyledGameLayout>
  );
};

export default GameLayout;
