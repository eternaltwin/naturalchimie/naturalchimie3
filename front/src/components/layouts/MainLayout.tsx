import React from 'react';
import styled from 'styled-components';
import Header from 'src/components/ui/Header/Header';
import LeftMenu from 'src/components/ui/LeftMenu/LeftMenu';
import assetsDictionnary from 'src/utils/assetsDictionnary';

const StyledLayout = styled.div`
  background-image: url(${assetsDictionnary['mainUi.mainBackground']});
  background-repeat: no-repeat;
  background-position: top center;
  background-color: #4c4236;
  min-height: 100vh;
  min-width: 100vw;
`;

const MainContainer = styled.div`
  border-radius: 5px;
  padding: 5px 20px;
  margin-left: auto;
  margin-right: auto;
  width: 65%;
`;

const Content = styled.div`
  display: flex;
  justify-content: center;
  background-color: #1a1612;
`;

const StyledLeftMenu = styled(LeftMenu)`
  width: 20%;
`;

const MainLayout: React.FC<{ C: React.ReactElement }> = ({ C }) => {
  return (
    <StyledLayout>
      <MainContainer>
        <Header />
        <Content>
          <StyledLeftMenu />
          {C}
        </Content>
      </MainContainer>
    </StyledLayout>
  );
};

export default MainLayout;
