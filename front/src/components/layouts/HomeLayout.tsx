import React, { useState } from 'react';
import styled from 'styled-components';
import DescriptionCard from '../ui/MainPage/DescriptionCard';
import IllustrationCard from '../ui/MainPage/IllustrationCard/IllustrationCard';
import RightMenu from '../ui/MainPage/RightMenu';
import MainLayout from './MainLayout';

const Center = styled.div`
  width: 60%;
  max-width: 550px;
  position: relative;
`;

const StyledDescriptionCard = styled(DescriptionCard)`
  margin: 2px auto;
  min-width: 90%;
`;

const StyledIllustrationCard = styled(IllustrationCard)`
  margin: 5px 8px 10px 10px;
`;

const HomeLayout: React.FC = () => {
  const [isTalking, setIsTalking] = useState<boolean>(false); //is the player talking to an NPC
  const [isMoving, setIsMoving] = useState<boolean>(false); //is the player attempting to move
  return (
    <MainLayout
      C={
        <>
          <Center>
            <StyledIllustrationCard isMoving={isMoving} />
            <StyledDescriptionCard />
          </Center>
          <RightMenu isTalking={isTalking} isMoving={isMoving} setIsMoving={setIsMoving} />
        </>
      }
    />
  );
};

export default HomeLayout;
