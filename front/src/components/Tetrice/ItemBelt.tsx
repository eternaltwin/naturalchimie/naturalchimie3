import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import assetsDictionnary from 'src/utils/assetsDictionnary';
import { useUserContext } from 'src/hooks/userContext';
import { useGameState } from 'src/hooks/GameStateContext';

const StyledItemBelt = styled.div`
  background-image: url(${assetsDictionnary['game.belt.background']});
  background-position: center;
  background-repeat: no-repeat;
  background-size: 92px 92px;
  position: relative;
  height: 95px;
  width: 95px;
`;

interface StyledImgProps {
  zindex: number;
  margin?: { top: number; left: number };
  zoomable?: boolean;
}

const StyledImg = styled.img<StyledImgProps>`
  position: absolute;
  margin-top: ${(props) => (props.margin ? props.margin.top + 'px' : '0px')};
  margin-left: ${(props) => (props.margin ? props.margin.left + 'px' : '0px')};
  z-index: ${(props) => props.zindex};
  height: 46px;
  width: 46px;
  ${(props) =>
    props.zoomable
      ? ` transition: transform 0.2s;
  :hover {
    transform: scale(1.15);
  }`
      : ``};
`;

const ItemBelt: React.FC = () => {
  const { itemBelt } = useUserContext();
  const [beltContent, setBeltContent] = useState<string[]>([]);
  const { useItemFromBelt } = useGameState();

  useEffect(() => {
    const lockedBelt = [
      assetsDictionnary['game.belt.pocket1'],
      assetsDictionnary['game.belt.pocket2'],
      assetsDictionnary['game.belt.pocket3'],
      assetsDictionnary['game.belt.pocket4'],
    ];
    const updatedBelt = lockedBelt.map((slot, index) => {
      const beltSlot = itemBelt[index];
      if (beltSlot.isLocked == true) {
        return slot;
      } else if (beltSlot.item) {
        // null case handled, eslint is just being dumb
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        return beltSlot.item!.img;
      } else return '';
    });
    setBeltContent(updatedBelt);
  }, [itemBelt]);

  const useItem = (itemIndex: number) => {
    if (itemIndex < itemBelt.length && itemBelt[itemIndex] != null) {
      useItemFromBelt(itemBelt[itemIndex]);
      setBeltContent(beltContent.map((slot, index) => (index === itemIndex ? '' : slot)));
    }
  };

  return (
    <StyledItemBelt>
      {beltContent.map((itemImg, index) =>
        itemImg !== '' ? (
          <StyledImg
            key={index}
            margin={{
              top: 2 + 46 * parseInt(index.toString(2).padStart(2, '0').split('')[0]),
              left: 2 + 46 * parseInt(index.toString(2).padStart(2, '0').split('')[1]),
            }}
            height={46}
            width={46}
            src={itemImg}
            zindex={1}
            zoomable={index < itemBelt.length}
            onClick={() => useItem(index)}
          />
        ) : (
          <div style={{ position: 'absolute', height: '46px', width: '46px' }} />
        )
      )}
    </StyledItemBelt>
  );
};

export default ItemBelt;
