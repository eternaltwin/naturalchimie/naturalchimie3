import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

import { useGameState } from 'src/hooks/GameStateContext';
import { AlchemyElement, GameItem } from 'src/types';
import assetsDictionnary from 'src/utils/assetsDictionnary';
import AnimatedCanvas, { AnimatedCanvasProps } from '../Canvas/AnimatedCanvas';
import Canvas, { CanvasProps } from '../Canvas/Canvas';
import { handleDropDraw } from './NextPiecesDrawingService';

const StyledNextPieces = styled.div`
  position: relative;
  height: 95px;
  width: 95px;
`;

interface StyledAnimatedCanvasProps extends AnimatedCanvasProps {
  zindex: number;
  top?: number;
  left?: number;
}

interface StyledCanvasProps extends CanvasProps {
  zindex: number;
  top?: number;
  left?: number;
}

const StyledCanvas = styled(Canvas)<StyledCanvasProps>`
  position: absolute;
  top: ${(props) => (props.top ? props.top + 'px' : '0px')};
  left: ${(props) => (props.left ? props.left + 'px' : '0px')};
  z-index: ${(props) => props.zindex};
`;

const StyledAnimatedCanvas = styled(AnimatedCanvas)<StyledAnimatedCanvasProps>`
    position: absolute;
    margin: 10px;
    top: ${(props) => (props.top ? props.top + 'px' : '0px')};
    left: ${(props) => (props.left ? props.left + 'px' : '0px')};
    z-index: ${(props) => props.zindex};
    animation-duration: 0.5s;
    animation-name: ${(props) => 'slide-in-' + props.top};
    animation-iteration-count: 1;
    @keyframes ${(props) => 'slide-in-' + props.top} {
    from {
        top: 12%;
    }

    to {
        top: ${(props) => (props.top ? props.top + 'px' : '0px')};
    }
`;

const NextPieces: React.FC = () => {
  const [currentPieces, setcurrentPieces] = useState<Array<AlchemyElement | GameItem>>([]);
  const { nextPieces } = useGameState();
  const [isAnimated, setIsAnimated] = useState(false);

  const handlePiecesDraw = (ctx: CanvasRenderingContext2D, frameCount: number) => {
    const offset = { vOffset: -6, hOffset: 10 };
    if (currentPieces) {
      handleDropDraw(ctx, frameCount, currentPieces, offset);
      if (((frameCount * 8) % ctx.canvas.height) + (currentPieces.length <= 2 ? offset.vOffset : 0) >= 45) {
        setIsAnimated(false);
      }
    }
  };

  const handleBackgroundDraw = (ctx: CanvasRenderingContext2D) => {
    const bgImage = new Image();
    bgImage.onload = () => ctx.drawImage(bgImage, 0, 0, 95, 95);
    bgImage.src = assetsDictionnary['game.nextPieceBackground'];
  };
  const handleForegroundDraw = (ctx: CanvasRenderingContext2D) => {
    const bgImage = new Image();
    bgImage.onload = () => ctx.drawImage(bgImage, 0, 0, 95, 95);
    bgImage.src = assetsDictionnary['game.nextPieceForeground'];
  };

  useEffect(() => {
    setcurrentPieces(
      nextPieces[0].map((piece) => ({
        ...piece,
      }))
    );
    setIsAnimated(true);
  }, [nextPieces]);

  return (
    <StyledNextPieces>
      <StyledCanvas
        height={95}
        width={95}
        draw={(ctx: CanvasRenderingContext2D) => handleBackgroundDraw(ctx)}
        zindex={0}
      />
      <StyledAnimatedCanvas
        height={80}
        width={80}
        draw={(ctx, frameCount) => handlePiecesDraw(ctx, frameCount)}
        playAnimation={isAnimated}
        zindex={1}
      />
      <StyledCanvas
        height={95}
        width={95}
        draw={(ctx: CanvasRenderingContext2D) => handleForegroundDraw(ctx)}
        zindex={2}
      />
    </StyledNextPieces>
  );
};

export default NextPieces;
