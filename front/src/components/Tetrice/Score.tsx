import React from 'react';
import styled from 'styled-components';
import { useGameState } from 'src/hooks/GameStateContext';

const StyledText = styled.span`
  display: block;
  position: absolute;
  height: 17px;
  width: 96px;
  text-align: center;
  font-family: 'LondonTwo';
  font-size: 14px;
  text-shadow: 0px 0px 0px #826e5c, -1px -1px 1px #ead5b2, 1px 1px 0px #4c4136, -2px -2px 2px rgba(76, 66, 54, 1);
  color: transparent;
  padding-left: 5px;
  padding-top: 5px;
  ::-moz-selection {
    background: #5af;
    color: #fff;
    text-shadow: none;
  }
  ::selection {
    background: #5af;
    color: #fff;
    text-shadow: none;
  }
`;

const StyledScore = styled.div`
  background: linear-gradient(to bottom left, #878f97 80%, #9ba6b2);
  border-top-left-radius: 1px;
  border-bottom-left-radius: 4px;
  height: 23px;
  width: 98px;
  #ScoreBackground {
    display: block;
    position: absolute;
    height: 17px;
    width: 96px;
    text-align: center;
    background: linear-gradient(to bottom left, #676f77, #77808a);
    margin-left: 3px;
    margin-bottom: 4px;
    border-top: solid #57565a 3px;
    border-top-left-radius: 2px;
    border-top-right-radius: 2px;
  }
`;

const Score: React.FC = () => {
  const { score } = useGameState();
  return (
    <StyledScore>
      <div id="ScoreBackground" />

      <StyledText>{score}</StyledText>
    </StyledScore>
  );
};

export default Score;
