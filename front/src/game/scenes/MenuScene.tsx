import { Scene } from 'phaser';
import assetsDictionnary from 'src/utils/assetsDictionnary';
import na3Game from '../game/na3Game';

export default class MenuScene extends Scene {
  game!: na3Game;
  constructor() {
    super({ key: 'MenuScene' });
  }

  preload(): void {
    this.load.image('gameBackground', this.game.currentLocation.background);
    this.load.image('gameForeground', assetsDictionnary['game.gameboard.foreground']);
  }
  create(): void {
    this.add.image(this.game.canvas.width, this.game.canvas.height / 2, 'gameBackground');
    this.add.image(this.game.canvas.width / 2 + 0.5, this.game.canvas.height / 2, 'gameForeground');
  }
}
