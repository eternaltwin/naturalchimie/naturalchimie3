import { Scene } from 'phaser';
import assetsDictionnary from 'src/utils/assetsDictionnary';
import na3Game from '../game/na3Game';

export default class BootScene extends Scene {
  game!: na3Game;
  constructor() {
    super({ key: 'BootScene' });
  }

  preload(): void {
    this.load.image('background', assetsDictionnary['game.gameboard.background']);
  }

  create(): void {
    this.add.image(this.game.canvas.width / 2, this.game.canvas.height / 2, 'background');
    this.input.on(
      'pointerdown',
      () => {
        this.scene.start('MenuScene');
      },
      this
    );
  }
}
