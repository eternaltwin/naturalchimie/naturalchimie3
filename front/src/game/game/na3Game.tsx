import { Game, Types } from 'phaser';
import { CurrentLocationContextType } from 'src/hooks/CurrentLocationContext';
import { GameStateContextTypes } from 'src/hooks/GameStateContext';

export default class na3Game extends Game {
  gameState!: GameStateContextTypes;
  currentLocation!: CurrentLocationContextType;
  constructor(config: Types.Core.GameConfig) {
    super(config);
  }

  setGameState(gameState: GameStateContextTypes): void {
    this.gameState = gameState;
  }
  setCurrentLocation(currentLocation: CurrentLocationContextType): void {
    this.currentLocation = currentLocation;
  }
}
