import React from 'react';
import InventoryLayout from '../components/layouts/InventoryLayout';

const Home: React.FC = () => {
  return <InventoryLayout />;
};

export default Home;
