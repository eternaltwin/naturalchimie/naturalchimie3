import React, { createContext, useContext, useMemo, useState } from 'react';
import { Belt, BeltSlot, Quest, Inventory } from 'src/types';
import { useIntl } from 'react-intl';
import generateItemBelt from 'src/utils/testData/SampleBelt';
import generateInventory from 'src/utils/testData/SampleInventory';

//Fake quest for testing
const quest: Quest = {
  name: "Loraine, on t'aime.",
  isActive: true,
  steps: [{ id: 1, description: 'déclarer son amour inconditionel à Loraine Ypsum', isCompleted: false }],
};

export interface UserContextType {
  userName: string;
  level: number;
  currency: number;
  pyrams: number;
  hasApa: boolean;
  inventory: Inventory;
  itemBelt: Belt;
  quests: Quest[];
  updateBeltContent: (usedItem: BeltSlot) => void;
  removeUsedItemFromBelt: (beltSlotNumber: number) => void;
}

export const UserContext = createContext<UserContextType | null>(null);

export const UserContextProvider: React.FC = ({ children }) => {
  const { formatMessage } = useIntl();
  const [userName, setUserName] = useState('');
  const [level, setLevel] = useState(0);
  const [currency, setCurrency] = useState<number>(0);
  const [pyrams, setPyrams] = useState<number>(0);
  const [hasApa, setHasApa] = useState(false);
  const [quests, setQuests] = useState<Quest[]>([quest]);
  const [inventory, setInventory] = useState<Inventory>(generateInventory(formatMessage));
  const [itemBelt, setItemBelt] = useState<Belt>(generateItemBelt(formatMessage));

  const removeUsedItemFromBelt = (beltSlotNumber: number) => {
    const usedItem = itemBelt.find((beltSlot) => beltSlotNumber === beltSlot.pocketIndex)?.item;

    //remove the item from the inventory
    setInventory(
      inventory.map((inventoryItem) =>
        inventoryItem.item === usedItem && inventoryItem.availableForBelt !== undefined
          ? { ...inventoryItem, availableForBelt: inventoryItem.availableForBelt - 1 }
          : inventoryItem
      )
    );
    //empty the pocket
    setItemBelt(
      itemBelt.map((beltSlot) => (beltSlotNumber === beltSlot.pocketIndex ? { ...beltSlot, item: null } : beltSlot))
    );
  };

  const updateBeltContent = (newItem: BeltSlot) => {
    //TODO: call the backend to remove item from belt
    const oldItem = itemBelt.find((beltSlot) => newItem.pocketIndex === beltSlot.pocketIndex)?.item;

    //add back the item to the inventory
    setInventory(
      inventory.map((inventoryItem) => {
        if ((!oldItem || inventoryItem.item.id === oldItem.id) && inventoryItem.availableForBelt !== undefined) {
          return { ...inventoryItem, availableForBelt: inventoryItem.availableForBelt + 1 };
        } else if (inventoryItem.item === newItem.item && inventoryItem.availableForBelt !== undefined) {
          return { ...inventoryItem, availableForBelt: inventoryItem.availableForBelt - 1 };
        } else {
          return inventoryItem;
        }
      })
    );
    //put the new item in the inventory
    setItemBelt(itemBelt.map((item) => (newItem.pocketIndex === item.pocketIndex ? newItem : item)));
  };

  const values = useMemo(
    () => ({
      userName,
      level,
      inventory,
      currency,
      pyrams,
      quests,
      setInventory,
      itemBelt,
      setItemBelt,
      removeUsedItemFromBelt,
      updateBeltContent,
      hasApa,
    }),
    [userName, level, inventory, itemBelt, hasApa, quests]
  );
  return <UserContext.Provider value={values}>{children}</UserContext.Provider>;
};

export const useUserContext = (): UserContextType => {
  const context = useContext(UserContext);
  if (context === undefined && context === null) {
    throw new Error('useGameState is missing UserContextContext');
  }
  return context as UserContextType;
};

export default UserContextProvider;
