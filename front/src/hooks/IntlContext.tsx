import React, { createContext, useContext, useMemo, useState } from 'react';
import { IntlProvider } from 'react-intl';
import French from 'src/utils/languages/fr/messages';
import flattenMessages from 'src/utils/messagesUtils';

const local = navigator.language;

// Not worth figuring out react-intl specific types
// eslint-disable-next-line
const chooseLanguage = (local: string): Record<string, any> => {
  let language;
  if (/fr(-[A-Z][A-Z])?/.test(local)) {
    language = French;
  } else {
    language = French;
  }
  return language;
};

interface LocaleContextType {
  locale: string;
  setLanguage: (locale: string) => void;
}

const LocaleContext = createContext<LocaleContextType | null>(null);

export const LocaleContextProvider: React.FC = ({ children }) => {
  const [locale, setLocale] = useState(local);

  const setLanguage = (newLocale: string) => {
    setLocale(newLocale);
  };

  const values = useMemo(
    () => ({
      locale,
      setLanguage,
    }),
    [locale]
  );

  return (
    <LocaleContext.Provider value={values}>
      <IntlProvider locale={locale} messages={flattenMessages(chooseLanguage(locale))}>
        {children}
      </IntlProvider>
    </LocaleContext.Provider>
  );
};

export const useLocale = (): LocaleContextType => {
  const context = useContext(LocaleContext);
  if (context === undefined && context === null) {
    throw new Error('useIntl is missing IntlContext');
  }
  return context as LocaleContextType;
};

export default LocaleContextProvider;
