import { Location } from 'src/types';
import assetsDictionnary from '../assetsDictionnary';
import generateElementChain from './SampleAlchemyChain';

const generateLocation = (formatMessage: (arg0: { id: string }) => string): Location => {
  return {
    name: 'Les Plaines de Loraine Ypsoum',
    description: `Vastes prairies pixelées où les développeurs
         de tous bords se rencontrent pour échanger des conseils et des cris de frustration.
        Les plaines de Loraine Ypsoum sont appelées ainsi en hommage à la première développeuse Géminite à hacker
        un chaudron pour tricher à son examen de transmutation. La Korki Gru de l'époque aurait dit: 
        "Je devrais être fâchée, mais je suis juste impressionnée."`,
    playerElements: [
      ...generateElementChain(formatMessage).splice(0, 11),
      {
        id: 0,
        description: 'unknown',
        rank: 11,
        value: 0,
        img: assetsDictionnary['elements.unknown'],
        usable: false,
      },
    ],
    completeChain: generateElementChain(formatMessage),
    background: assetsDictionnary['locations.prairieBackground'],
    isSchoolCup: false,
    areMagicItemsAllowed: true,
  };
};

export default generateLocation;
