/* @font-face kit by Fonts2u (https://fonts2u.com) */
import scoreFontLayerEot from 'src/assets/font/LondonTwo.eot';
import scoreFontLayerSvg from 'src/assets/font/LondonTwo.svg';
import scoreFontLayerTtf from 'src/assets/font/LondonTwo.ttf';
import scoreFontLayerWoff from 'src/assets/font/LondonTwo.woff';

// eslint-disable-next-line
const assetsDictionnaryFonts: Record<string, any> = {
  scoreLayerEot: scoreFontLayerEot,
  scoreLayerSvg: scoreFontLayerSvg,
  scoreLayerTtf: scoreFontLayerTtf,
  scoreLayerWoff: scoreFontLayerWoff,
};

export default assetsDictionnaryFonts;
