import flattenMessages from 'src/utils/messagesUtils';

import elementsDictionnary from 'src/utils/assetsDictionnaries/elements';
import fontsDictionnary from 'src/utils/assetsDictionnaries/fonts';
import gameDictionnary from 'src/utils/assetsDictionnaries/game';
import locationBackgroundsDictionnary from 'src/utils/assetsDictionnaries/locationBackgrounds';
import mainPageDictionnary from 'src/utils/assetsDictionnaries/mainUi';
import inventoryPageDictionnary from 'src/utils/assetsDictionnaries/inventoryPage';
import map from 'src/assets/img/locations/map.png';

// No way to get around an any here
// eslint-disable-next-line
const assetsDictionnary: Record<string, any> = {
  elements: elementsDictionnary,
  fonts: fontsDictionnary,
  inventory: inventoryPageDictionnary,
  game: gameDictionnary,
  locations: locationBackgroundsDictionnary,
  mainUi: mainPageDictionnary,
  map: map,
};

export default flattenMessages(assetsDictionnary);
