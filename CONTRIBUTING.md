# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue,
or [Discord](https://discord.gg/scxSvuSNJs) before making a change. 
If you are working on an open ticket from the [Trello](https://naturalchimie.atlassian.net), this step isn't required.

Please note we have a code of conduct, please follow it in all your interactions with the project.

## Pull Request Process

1- Please make any changes on a local branch from the development branch, and prefix the branch name by feature- for a new feature and fix- for a fix:
```
git checkout -b feature-roue-chouettex
```
Or for a fix
```
git checkout -b fix-remove-pepite-dropping
```
2- Verify coding style before creating the merge request

3- The pipeline must pass for a merge request to be merged. So make sure your branch builds properly and that unit tests pass!

4- Issue a Merge Request describing summarizing what you have done, wait for the approval of an other developper before merging if merging to develop, or three developers if merging to master. If no one is coming to approve the MR, use Discord to communicate with the team! Please use a template to label your merge request.

All changes **must** be merged to develop or a release- branch first. The master branch can only receive a merge from two sources:
1- A merge request from develop (when releasing a version, for example)

2- A merge request from a release- branch where fixes have been done on a specific release of the app and are merged back into master and develop.
