# Projet NaturalChimie 3

Reprenant NaturalChimie 2. Toutes les images présentes dans assets/img sont la propriété de Motion-Twin, sous license [CC-BY-NC-SA-4.0](https://spdx.org/licenses/CC-BY-NC-SA-4.0.html). Le reste du projet est sous license [AGPL-3.0-or-later](https://spdx.org/licenses/AGPL-3.0-or-later.html)

## Installer l'environnement de dev
Sont nécessaires :
* npm
* chocolatey
* make (choco install make)
* Docker Desktop (https://docs.docker.com/get-docker/)

### Quick-install de l'environnement de dev
Le script install_dev_env.ps1 peut rapidement installer les outils nécessaires au développement et build sur Windows. Le script installe actuellement git, npm, chocolatey, et Docker Desktop. Il n'est actuellement pas testé (j'ai déjà tout sur mon poste. :calim:)

### Install via Docker et Make
Copier le fichier .env.example et le coller en un fichier .env

Pour initialiser le projet en local 
* make install


## Lancer Naturalchimie 3
### Lancer le serveur Eternal-Twin en local
Première étape : Copier le fichier etwin.toml.example en etwin.toml. 
#### Avec Docker et Makefile

Pour lancer le serveur Eternal-Twin en local :
* make run-etwin

#### Avec npm seul
Dans le dossier etwin :
* npm install 
* npm run etwin

Il faut avoir un Postgre lancé sur le port 5432 pour ça, ou changer la ligne `api = "postgres"`en `api = "in-memory"` dans le fichier toml.
Le serveur Eternal-Twin sera lancé sur localhost:50320/.

### Lancer le serveur Naturalchimie en dev (sans compilation)
#### Avec Docker et Makefile

Lancer le back :
* make run-back

Lancer le front :
* make run-front


#### Avec npm seul

Dans le dossier back :

* npm install
* npm run start:dev

Nodemon lancera le serveur et le mettra à jour automatiquement lors des changements des fichiers.


#### Utils
## Docker + Makefile

Voir la doc du fichier Makefile ainsi que toute les commandes associées

* make

Réinitialiser le projet en local

* make reset

## Troubleshooting
### Changement des ports

Tout les ports de la machine local sont géré via le fichier .env
En cas de conflit, changez les ports dans ce fichier.

### Port 50320 indisponible pour Docker
Le service Windows d'assignation NAT peut prendre le port 50320, ce qui empêche Docker de lancer le container.
Un workaround simple est d'exécuter les commandes suivantes :
```
net stop winnat
make install-eternal-twin
net start winnat
```

