DOCKER           = docker
DOCKER_COMPOSE   = docker-compose

EXEC_FRONT       = $(DOCKER_COMPOSE) exec -T na3_front /entrypoint
EXEC_BACK        = $(DOCKER_COMPOSE) exec -T na3_back /entrypoint
EXEC_ETWIN       = $(DOCKER_COMPOSE) exec -T na3_etwin /entrypoint
EXEC_DB       	 = $(DOCKER_COMPOSE) exec -T na3_db



##
##PROJECT
##-------

build: ## Build the project
build: 
	@$(DOCKER_COMPOSE) pull --parallel --quiet --ignore-pull-failures 2> /dev/null
	$(DOCKER_COMPOSE) build --pull


kill: ## Remove containers
kill:
	$(DOCKER_COMPOSE) kill
	$(DOCKER_COMPOSE) down --volumes --remove-orphans

install: ## Install and start the project
install: build start install-back install-front install-etwin


reset: ## Stop and start a fresh install of the project
reset: clean install

clean: ## Stop the project and remove generated files
clean: kill
	rm -rf ./back/node_modules ./front/node_modules

start: ## Start the project
	$(DOCKER_COMPOSE) up -d --remove-orphans --no-recreate

watch: ## Start without detached
watch:
	$(DOCKER_COMPOSE) up

stop: ## Stop the project
	$(DOCKER_COMPOSE) stop

test: ## Test the project
test: test-front test-back

lint: ## Lint the entire project
lint: lint-front lint-back

.PHONY: build kill install reset clean start stop lint
	
##
##-----------------------------------------------------------------
##

##FRONT
##-----

node_modules-front: ## 
node_modules-front: ./front/yarn.lock
	$(EXEC_FRONT) yarn install

bash-front: ## 
bash-front: 
	$(EXEC_FRONT) bash



##
##install

install-front: ## 
install-front: node_modules-front
	
	
##
##run
run-front: ## 
run-front: 
	$(EXEC_FRONT) yarn start

##
##test
test-front: ##
test-front:
	#$(EXEC_FRONT) npm test
	echo TODO

##
##lint
lint-front: ##
lint-front:
	$(EXEC_FRONT) yarn lint

.PHONY: bash-front run-front install-front lint-front


##
##-----------------------------------------------------------------
##

##BACK
##-----

node_modules-back: ## 
node_modules-back: ./back/package-lock.json
	$(EXEC_BACK) npm install

bash-back: ## 
bash-back:
	$(EXEC_BACK) bash

##
##install

install-back: ## 
install-back: node_modules-back

##
##run
run-back: ## 
run-back: 
	$(EXEC_BACK) npm run start

##
##test
test-back: ##
test-back:
	$(EXEC_BACK) npm test
	
##
##lint
lint-back: ##
lint-back:
	$(EXEC_BACK) npm run lint
	

.PHONY: bash-back run-back install-back lint-back
##
##-----------------------------------------------------------------
##

##ETWIN
##-----

install-etwin: ##
install-etwin: reset-db
	$(EXEC_ETWIN) npm install

run-etwin: ##
run-etwin:
	$(EXEC_ETWIN) npm run etwin

.PHONY: install-etwin run-etwin
##
##-----------------------------------------------------------------
##

##DB
##-----

bash-db: ## 
bash-db:
	$(EXEC_DB) bash

reset-db: ## 
reset-db: start
	cat docker/etwin/drop.sql | docker exec -i na3_db psql --username mysql eternal_twin &&\
	cat docker/etwin/dump_12-01-2021_20_33_41.sql | docker exec -i na3_db psql --username mysql eternal_twin

.PHONY: bash-db reset-db

##
##-----------------------------------------------------------------
##

##DOCKER
##-----

remove-all: ## Warning, it will remove EVERY container, images, volumes and network not only na3 ones
remove-all: 
	$(DOCKER) system prune --volumes -a


##
##-----------------------------------------------------------------
##

##UTILS
##-----


##
##-----------------------------------------------------------------
##

##CI
##-----

ci: ## Trigger ci for dependencies cache
ci:	start ci-front ci-back

ci-front: ## 
ci-front: install-front
	

ci-back: ## 
ci-back: install-back


.PHONY: ci ci-front ci-back
##
##logs

logs-front: ## Access logs front container
logs-front:
	$(DOCKER_COMPOSE) logs front

.PHONY: logs-front

##
##doc
.DEFAULT_GOAL := doc
doc: ## List commands available in Makefile
doc:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: doc

