#!/bin/sh

username='dev'
group='dev'


uid=$(stat -c %u /www)
gid=$(stat -c %g /www)

if [ $uid == 0 ] && [ $gid == 0 ]; then
    if [ $# -eq 0 ]; then
        sleep 9999d
    else
        exec "$@"
    fi
fi

sed -i -r "s/node:x:1000:1000:Linux/node:x:100:100:Linux/g" /etc/passwd

sed -i -r "s/${username}:x:\d+:\d+:/${username}:x:$uid:$gid:/g" /etc/passwd
sed -i -r "s/${username}:x:\d+:/${username}:x:$gid:/g" /etc/group
chown ${username} /home
echo test
if [ $# -eq 0 ]; then
    sleep 9999d
else
    exec su-exec ${username} "$@"
fi
