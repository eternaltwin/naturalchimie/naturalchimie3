import locations from '../assets/locations.json'

export class Location {
  public static getLocation (id: number) {
    return locations.locations[id]
  }

  public static getTuples (id: number) {
    return locations.locations[id].tuples
  }

  public static getChain (id: number) {
    return locations.locations[id].chain
  }

  public static getLocationIdByName (name: string) {
    for (let i = 0; i < locations.locations.length; i++) {
      if (locations.locations[i].name === name) {
        return i
      }
    }
    return -1
  }

  public static getMaxElementForTuple (id: number, current: number) {
    const indexOfElement = locations.locations[id].chain.findIndex((e:number) => e === current)
    if (indexOfElement !== null && indexOfElement !== -1) {
      // We can't have a top-level element in the tuple, so return 10 instead
      return indexOfElement <= 10 ? indexOfElement : 10
    } else {
      // We didn't find the element in the chain, return green pot just in case
      return 0
    }
  }
}
