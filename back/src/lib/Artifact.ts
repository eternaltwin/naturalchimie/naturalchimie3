export class Artifact {
private id : number

// List of alchimical items that stay on screen instead of having a single action
// TODO: Put actual values :-)
private persistentItems : number[] = [29, 30]

public constructor (inId: number) {
  this.id = inId
}

public isElement () {
  return this.id < 29
}

public causesLoss () {
  return this.isElement() || this.isPersistentItem()
}

public isPersistentItem () { // Skat, pyrite, corbeau, etc...
  return this.persistentItems.includes(this.id)
}

public getId () {
  return this.id
}
}
