import { Artifact } from './Artifact'

export class BoardState {
private state : Artifact[][]

public constructor (private initState: Artifact[][]) {
  this.state = initState
}

public isLegal () {
  const dimensions = [
    this.state.length,
    // Take the biggest dimension between 0 or dimension size (expected: 6)
    this.state.reduce((x, y) => Math.max(x, y.length), 0),
    // Take the smallest dimension between 7 or dimension size (expected: 6)
    this.state.reduce((x, y) => Math.min(x, y.length), 7)
  ]
  if (dimensions[0] !== 9 || dimensions[1] !== dimensions[2] || dimensions[1] !== 6) {
    return false
  }
  return true
}

public isLost () {
  const aboveLine:Artifact[] = this.state[1]
  if (aboveLine.some(e => e.getId() >= 0 && e.causesLoss())) {
    return true
  }
  return false
}

public getState () {
  return this.state
}

public getHighestElement () {
  return Math.max(...this.state.flat()
  // Remove all non-elements
    .filter((e:Artifact) => e.isElement())
  // Get the id
    .map((e:Artifact) => e.getId()))
}
}
