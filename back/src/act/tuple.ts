import { BoardState } from '../lib/BoardState'
import { Artifact } from '../lib/Artifact'
import { Location } from '../lib/Location'
import { MathUtils } from '../lib/MathUtils'
import { express } from '../lib/express'

const bodyParser = require('body-parser')

// create application/json parser
const jsonParser = bodyParser.json()

const router = express.Router()

router
  .post('/', jsonParser, function (request: any, response: any) {
    const board = new BoardState(request.body.board.map((e:any) => e.map((a:any) => new Artifact(a))))
    if (!board.isLegal()) {
      response.send("This board isn't legal...")
    } else if (board.isLost()) {
      response.send('Too bad!')
    } else {
      // TODO: send a generateElementalTuple with tupleSize 2 and optionals to default for just elements
      response.send('Next tuple is: ' + generateNextTuple(Location.getLocationIdByName('Plaine de l\'Armée'), board.getHighestElement()))
    }
  })

const elementWeight = [18, 18, 18, 18, 12, 8, 7, 5, 4, 1, 1]

export function generateNextTuple (locationId: number, highestElement: number) {
  // Get all potential tuples for the location and add up the weight
  const tuplePossibilities = Location.getTuples(locationId)
  const totalWeight = tuplePossibilities.reduce((a, b) => a + b.weight, 0)
  let randomValueForElement: number = MathUtils.getRandomIntInclusive(0, totalWeight)
  let i = 0
  // Get a random pick in that list
  for (i; tuplePossibilities[i].weight <= randomValueForElement; i++) {
    randomValueForElement = randomValueForElement - tuplePossibilities[i].weight
  }
  // If it's not elemental, we can just return the item directly. Otherwise generate tuple
  if (tuplePossibilities[i].elemental) {
    return generateElementalTuple(locationId, highestElement, tuplePossibilities[i].size, tuplePossibilities[i].item, tuplePossibilities[i].elemental)
  } else {
    return tuplePossibilities[i].item
  }
}

export function generateElementalTuple (locationId: number, highestElement: number, tupleSize: number, item: string = '', elemental = true) {
  const itemsUniqueForATuple : string[] = ['polarized_bomb', 'crow']
  const maxElement = Location.getMaxElementForTuple(locationId, highestElement)
  const totalWeight = elementWeight.slice(0, maxElement).reduce((a, b) => a + b, 0)
  const locationChain: number[] = Location.getChain(locationId)
  const tuple = []
  for (let i = 0; i < tupleSize; i++) {
    if (item !== '') {
      // There is ALWAYS one and only one polarized bomb in this tuple
      if (itemsUniqueForATuple.includes(item)) {
        // TODO: What is the actual formula? I'm artificially restricting it but this is a completely random value
        if ((i === 0 || (i === 1 && tuple[0] !== item)) && MathUtils.getRandomIntInclusive(1, 6) === 6) {
          tuple[i] = item
          continue
        }
      // TODO: wth is the actual formula??? In the meantime, we do this
      // For pyrite, skats and co, flip a coin: heads, we have a skat/pyrite/whatever at that position
      // This ensures having the possibility of 0, 1 or 2 skats in a double tuple
      } else if (MathUtils.getRandomIntInclusive(0, 1) === 1) {
        if (i > 0 && tuple[i - 1] === item) {
          if (MathUtils.getRandomIntInclusive(1, 10) === 10) {
            tuple[i] = item
            continue
          }
        } else {
          tuple[i] = item
          continue
        }
      }
    }
    let randomValueForElement: number = MathUtils.getRandomIntInclusive(0, totalWeight)
    let j = 0
    for (j; elementWeight[j] <= randomValueForElement; j++) {
      randomValueForElement = randomValueForElement - elementWeight[j]
    }
    tuple[i] = locationChain[j]
  }
  return tuple
}

module.exports = { router, generateElementalTuple, generateNextTuple }
