import { express } from '../lib/express'

const router = express.Router()

const tupleRoutes = require('./tuple').router

router
  .get('/', function () {
  // Render any /act routes if there are any
  })

  .use('/tuple', tupleRoutes)

module.exports = router
